# Unsupervised Learning and Dimensionality Reduction

# CS 7641 Assignment - 3

The purpose of this project is to explore below techniques in Unsupervised Learning and Dimensionality Reduction.

- K-Means
- Expectation Maximization
- PCA
- ICA
- Randomized Projections
- Random Forest

REPO Link: [https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment3](https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment3)
## Steps to run the code:

### Assignment 3:

- Download the code for assignment 3 from above Gitlab link.

- Both Datasets are present in assignment3/data folder.

- Assignment 3 folder has `run_experiment.py` with parameter ` --all` file which should be used to run all algorithms for Part 1 and Part 2.

- Execute `python run_experiment.py --all`

- Results can be plotted for Part 1 and 2 by running `python run_experiment.py --plot`

- Update the Dim values obtained from the scree graph from above experiment in `run_clustering.sh` file and execute the shell script for remaining parts of the assignment. 

- Execute `run_clustering.sh`

- Results can be plotted for the remaining parts of the assignment by running `python run_experiment.py --plot`

Above commands will take care of plotting the charts as well. The generated charts can be found under output/images folder.

## Credits:

Thanks to jontay and chad ([https://github.com/JonathanTay](https://github.com/JonathanTay)) and ([https://github.com/cmaron/CS-7641-assignments](https://github.com/cmaron/CS-7641-assignments)) for sharing their code, parts of those code have been used as part of this assignment.

## Author:

Name: Melvinpradep Gregory

Email: melvinpradep@gatech.edu

GT ID: mgregory36

