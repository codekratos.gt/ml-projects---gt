
# CS 7641 Assignment - 2 - Randomized Optimization



The purpose of this project is to explore below Optimization Algorithms.

- Random Hill Climbing
- Genetic Algorithm
- Simulated Annealing
- MIMIC

REPO Link: [https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment2](https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment2)

## Language:

- Python 3.6.0
- Jython 2.7.0

## Steps to run the code:

### Assignment 2:

- Download the code for assignment 2 from above Gitlab link.

- Dataset is present in assignment2/data folder in the repo.

- Install Packages - `pip install -r requirements.txt` should take care of the required packages.

- Assignment 2 folder has `run_experiment.py` file which should be used to create data dump


### NN - Optimization:

- Execute the command using python to create the data dump `python run_experiment.py --dump_data`

- Execute the command using jython to run NN Backpropagation - `jython NN-Backprop.py`

- Execute the command using jython to run NN GA - `jython NN-GA.py`

- Execute the command using jython to run NN SA - `jython NN-SA.py`

- Execute the command using jython to run NN RHC - `jython NN-RHC.py`


### Optimization Problem Domains:

- Execute the command using jython to run TSP Problem - `jython tsp.py`

- Execute the command using jython to run Continuous Peaks Problem - `jython continuouspeaks.py`

- Execute the command using jython to run Flip Flop Problem - `jython flipflop.py`


### Plotting

- Execute the command using python to create plotting for all results - `plotting.py`

Above command will take care of plotting the charts. The generated charts can be found under output/images folder inside assignment2.

## Credits:

Thanks to jontay and chad (https://github.com/JonathanTay) and (https://github.com/cmaron/CS-7641-assignments) for sharing their code, parts of those code have been used as part of this assignment.

## Author:

Name: Melvinpradep Gregory

Email: melvinpradep@gatech.edu

GT ID: mgregory36