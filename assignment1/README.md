# CS 7641 Assignment - 1

The purpose of this project is to explore below techniques in supervised learning.

- Decision trees with some form of pruning
- Neural networks
- Boosting
- Support Vector Machines
- K-Nearest Neighbors

REPO Link: [https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment1](https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment1)
## Steps to run the code:

### Assignment 1:

- Download the code for assignment 1 from above Gitlab link.

- Both Datasets are present in assignment1/data1 folder

- Install Packages - `pip install -r requirements.txt` should take care of the required packages.

- Assignment 1 folder has `run_experiment.py` file which should be used to run all 5 algorithms.

- DT - execute the following command to run the DT algorithm `python run_experiment.py --threads -1 --dt`

- Boosting - execute the following command to run the Boosting algorithm `python run_experiment.py --threads -1 --boosting`

- Neural Networks - execute the following command to run the Neural Networks algorithm `python run_experiment.py --threads -1 --ann`

- KNN - execute the following command to run the KNN algorithm `python run_experiment.py --threads -1 --knn`

- SVM - execute the following command to run the SVM algorithm `python run_experiment.py --threads -1 --svm`

- For all algorithms - execute the following command to run all algorithms `python run_experiment.py --threads -1 --all`

Above commands will take care of plotting the charts as well. The generated charts can be found under output/images folder.

## Credits:

Thanks to jontay and chad ([https://github.com/JonathanTay](https://github.com/JonathanTay)) and ([https://github.com/cmaron/CS-7641-assignments](https://github.com/cmaron/CS-7641-assignments)) for sharing their code, parts of those code have been used as part of this assignment.

## Author:

Name: Melvinpradep Gregory

Email: melvinpradep@gatech.edu

GT ID: mgregory36