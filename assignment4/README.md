# MDP - Markov Decision Processes

# CS 7641 Assignment - 4

The purpose of this project is to explore RL techniques to solve MDP problems.

- Policy Iteration
- Value Iteration
- Q-Learning


REPO Link: [https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment4](https://gitlab.com/codekratos.gt/ml-projects---gt/tree/master/assignment4)

## Language and Dependencies:

- Python 3.6.0
- Install the required packages using `requirements.txt`
## Steps to run the code:

### Assignment 4:


- Download the code for assignment 4 from above Gitlab link.

- All problems are present in assignment4/environments folder.

- Assignment 4 folder has `run_experiment.py` with parameter ` --all` file which should be used to run all algorithms for both problems.

- Execute `python run_experiment.py --all`

- Results can be plotted for all 3 algorithms by running `python run_experiment.py --plot`

Above commands will take care of plotting the charts as well. The generated charts can be found under assignment4/output folder.

## Credits:

Thanks to jontay, chad and michael ([https://github.com/JonathanTay](https://github.com/JonathanTay)), ([https://github.com/cmaron/CS-7641-assignments](https://github.com/cmaron/CS-7641-assignments)) and ([https://github.gatech.edu/mmallo3](https://github.gatech.edu/mmallo3)) for sharing their code, parts of those code have been used as part of this assignment.

## Author:

Name: Melvinpradep Gregory

Email: melvinpradep@gatech.edu

GT ID: mgregory36


